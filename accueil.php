<?php
session_start();
//Si le boolen n'est pas defini, c'est donc la premiere fois que vous venez -> le booleen connexion est initialisé à False
if (!isset($_SESSION['verified'])) {
    $_SESSION['verified'] = False;
    $_SESSION['origine'] = "accueil";

}
if (!isset($_POST["accueil"])) {
    $_SESSION['verified'] = False;
    $_SESSION['origine'] = "accueil";
}
?>
<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>Bulltozer</title>
    <link rel="stylesheet" href="css/main.css">
</head>




<body>

    <div class="grid-container">
        <div class="interactions">
                <form name="formulaire" action="accueil.php" method="POST">
                    <button type="submit" formaction="creer.php" name="creerAccueil" value="creerAccueil">Creer</button>
                    <button type="submit" formaction="gerer.php" name="gererAccueil" value="gererAccueil">Gerer</button>
                    <button type="submit" formaction="repondre.php" name="repondreAccueil" value="repondreAccueil">Repondre</button>
                    <button type="submit" formaction="login.php" name="loginAccueil" value="loginAccueil">Log in</button>
                    <button type="submit" formaction="register.php" name="registerAccueil" value="registerAccueil">Register</button>
                </form>
        </div>
        <div class="bulltozer">
            <h1>Bulltozer</h1>
            <h2>Accueil</h2>
            <?php
            if ($_SESSION['verified']) {
                echo "<p>Bonjour " . $_SESSION["personne"]["prenom"] . "</p>";
            } else {
                echo "<p>Vous etes déconnécté</p>";
            }
            ?>
        </div>
        <div class="account">
            <form name="sendToAccount" action="account.php" method="POST">
                <?php
                if ($_SESSION['verified']) {
                    echo "<button type='submit' id='accountButton' formaction='account.php' name='accountAccueil' value='account'><img src='img/connected.png'></button>";
                    echo"<button type='submit' formaction='accueil.php' name='disconnect' value='disconnect'>Disconnect</button>";
                } else {
                    echo "<button type='submit' id='accountButton' formaction='account.php' name='accountAccueil' value='account'><img src='img/disconnected.png'></button>";
                }
                ?>
            </form>
        </div>
    </div>

</body>

</html>