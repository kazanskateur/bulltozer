<?php
session_start();
$jsonString = file_get_contents('json/personne.json');
$personne = json_decode($jsonString, true);
function fetchPersonne($id, $personne)
{
    foreach ($personne as $var) {
        if ($id == $var["id"]) {
            return $var;
        }
    }
}
?>
<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>Bulltozer</title>
    <link rel="stylesheet" href="css/main.css">
</head>




<body>
    <header>
        <h1>Bulltozer</h1>
        <h2>Informations</h2>
    </header>

    <?php
    //On test ici si l'adresse mail renseigné est deja utilisé
    if ($_SESSION["verified"]) {
        if (isset($_POST["changer"])) {
            if (empty($_POST["email"]) || empty($_POST["nom"]) || empty($_POST["prenom"])) {
                echo "<p>Il faut remplir tout les champs</p>";
            } else {
                $emailList[] = array();
                foreach ($personne as $var) {
                    //On ne veut pas se soucier de l'adresse mail de notre utilisateur
                    if($var["id"]!=$_SESSION["personne"]["id"]){
                        $emailList[] = $var["email"];
                    }
                }
                //Si l'email existe deja dans la DB
                if(in_array($_POST["email"],$emailList)){
                    echo "Il faut choisir une autre email";
                }else{
                    foreach($personne as $key=>$var){
                        //On cherche l'id de la session avec celui de la connexion
                        if($var["id"]==$_SESSION["personne"]["id"]){
                            //Si on a pas renseigné de nouveau mot de passe on le recupere de la session
                            if(empty($_POST["password"])){
                                $personne[$key] = array("id" => $_SESSION["personne"]["id"], "nom" => $_POST["nom"], "prenom" => $_POST["prenom"], "email" => $_POST["email"], "password" => $_SESSION["personne"]["password"]);
                            }
                            //Sinon on le Rehash
                            else{
                                $personne[$key] = array("id" => $_SESSION["personne"]["id"], "nom" => $_POST["nom"], "prenom" => $_POST["prenom"], "email" => $_POST["email"], "password" => password_hash($_POST["password"], PASSWORD_DEFAULT));
                            }
                            $_SESSION["personne"]=$personne[$key];
                            //Puis on rencode et on redirige
                            $newJsonString = json_encode($personne, JSON_PRETTY_PRINT);
                            file_put_contents('json/personne.json', $newJsonString);
                            header('Location: success.php');
                            exit();
                        }
                    }
                }
            }
        }
    } else {
        header('Location: login.php');
        exit();
    }
    ?>

    <form name="connexion" action="account.php" method="POST">
        <table>
            <tr>
                <td colspan='2'>
                    <div class="labelClass">
                        <label for="nom">Nom</label>
                        <label for="prenom">Prenom</label>
                        <label for="email">Email</label>
                        <label for="password">Password</label>
                    </div>
                    <div class="inputClass">
                        <?php
                        //On prefere ici ecrire en dur au vu des types d'input
                        echo "
                        <input type='text' name='nom' value='" . $_SESSION["personne"]["nom"] . "'>
                        <input type='text' name='prenom' value='" . $_SESSION["personne"]["prenom"] . "'>
                        <input type='email' name='email' value='" . $_SESSION["personne"]["email"] . "'>
                        <input type='password' name='password' placeholder='password'>
                        ";
                        ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <button type="submit" name="changer" value="Changer">Changer</button>
                </td>
                <td>
                    <button type="submit" name="accueil" value="Accueil" formaction="accueil.php">Accueil</button>
                </td>

            </tr>
        </table>
    </form>
</body>

</html>