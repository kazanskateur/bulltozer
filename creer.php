<?php
session_start();
//On load les data su fichier values
$jsonString = file_get_contents('json/values.json');
$valeurs = json_decode($jsonString, true);
$jsonString = file_get_contents('json/personne.json');
$personne = json_decode($jsonString, true);
$jsonString = file_get_contents('json/personneQuestionnaire.json');
$personneQuestionnaire = json_decode($jsonString, true);
$jsonString = file_get_contents('json/bulletin.json');
$bulletin = json_decode($jsonString, true);

//Si c'est le bouton creer de la page accueil qui a mené ici on créé un tableau avec by default oui ou non
if (isset($_POST["creerAccueil"])) {
    $valeurs = array("oui", "non");
}

if (isset($_POST["creerAccueil"])) {
    $personneQuestionnaire = array();
}

function fetchPersonne($id, $personne)
{
    foreach ($personne as $var) {
        if ($id == $var["id"]) {
            return $var;
        }
    }
}

function fetchPersonneBool($id, $personne)
{
    foreach ($personne as $var) {
        if ($id == $var["id"]) {
            return True;
        }
    }
    return False;
}
?>
<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>Bulltozer</title>
    <link rel="stylesheet" href="css/main.css">
</head>


<body>

    <header>
        <h1>Bulltozer</h1>
        <h2>Creer un formulaire</h2>
    </header>


    <?php
    //Si verified false on redirige vers la page de login
    if (!$_SESSION["verified"]) {
        header('Location: login.php');
        exit();
    }
    //Ici on test si le formulaire a ete correctement rempli: Au moins deux options et au moins 3 personnes.
    //Si on a appuyé sur le bouton creer de la page
    if (isset($_POST["creer"])) {
        //Si il y'a plus de 3 personnes qui doivent repondres
        if (count($personneQuestionnaire) > 2) {
            //Si il y'a plus de deux options
            if (count($valeurs) > 1) {
                if (!empty($_POST["question"])) {
                    //Si il n'existe pas de bulletin
                    if (count($bulletin) == 0) {
                        $bulletin[] = array("id" => "0", "createur" => $_SESSION["personne"], "question" => $_POST["question"], "options" => $valeurs, "personnes" => $personneQuestionnaire, "votes" => array(), "complet" => False);
                    } else {
                        $bulletin[] = array("id" => (string)(end($bulletin)["id"] + 1), "createur" => $_SESSION["personne"], "question" => $_POST["question"], "options" => $valeurs, "personnes" => $personneQuestionnaire, "votes" => array(), "complet" => False);
                    }

                    $newJsonString = json_encode($bulletin, JSON_PRETTY_PRINT);
                    file_put_contents('json/bulletin.json', $newJsonString);
                    header('Location: success.php');
                    exit();
                } else {
                    echo "<p>Veuillez entrer une question</p>";
                }
            } else {
                echo "<p>Il n'y a pas assez d'options crées<br>Créez en au moins 2</p>";
            }
        } else {
            echo "<p>Il n'y a pas assez de personne séléctionées<br>Prenez en au moins 3</p>";
        }
    }
    ?>

    <?php
    //Ici on gere les ajouts et supression au JS des options de reponses
    //On parcours le tableau et on supprime la valeur si on trouve un bouton suppr déclenché.
    foreach ($valeurs as $key => $var) {
        if (isset($_POST["del" . $var])) {
            unset($valeurs[$key]);
        }
    }
    //Si un bouton add est declenché et qu'autre chose qu'une chaine vide est détéctée
    if (!isset($_POST["creerAccueil"])) {
        if (isset($_POST["Ajouter"])) {
            if (empty($_POST["valueAdd"])) {
                echo "<p>Une chaine vide n'est pas une option</p>";
            } elseif (in_array($_POST["valueAdd"], $valeurs)) {
                echo "<p>Cette option existe deja</p>";
            } else {
                $valueAdd = $_POST["valueAdd"];
                $valeurs[] = $valueAdd;
            }
        }
    }
    ?>


    <?php
    //Ici on gere la supression et l'ajout au JSON des personnes participant au sondage.
    //On parcours le tableau et on supprime la valeur si on trouve un bouton suppr déclenché.
    foreach ($personneQuestionnaire as $key => $var) {
        $chaine = "del" . $var["id"];
        if (isset($_POST[$chaine])) {
            unset($personneQuestionnaire[$key]);
        }
    }
    //On regarde si on a appuyé sur un bouton addPersonne et si people et defini et si la personne n'existe pas deja dans la liste des personnes a repondre
    if (isset($_POST["addPersonne"]) && isset($_POST["people"]) && !fetchPersonneBool($_POST["people"], $personneQuestionnaire)) {
        $id = $_POST["people"];
        $personneQuestionnaire[] = fetchPersonne($id, $personne);
    }
    ?>


    <form name="bulletinSettings" action="creer.php" method="POST">
        <table id="bulletinSettings">
            <?php
            //On garde la question precedemment rentré
            if (isset($_POST["question"]) && $_POST["question"] !== "") {
                echo '<tr>
                <td colspan=2>
                    <textarea class="inputCentre" name="question" placeholder="Question">'.htmlspecialchars($_POST["question"]).'</textarea>
                </td>
                </tr>';
            } else {
                echo "<tr>
                <td colspan=2>
                    <textarea class='inputCentre'  name='question' placeholder='Question'></textarea>
                </td>
                </tr>";
            }

            //Affichage de valeurs et des boutons
            if ($valeurs != null) {
                echo "<tr><td colspan = '2'>
                <div class='labelClass'>
                ";
                //On affiche tout les labels
                foreach ($valeurs as $var) {
                    echo "<label for='del" . $var . "'>" . $var . "</label>";
                }
                echo "
                </div>
                <div class='inputClass'>
                ";
                //Puis on affiche tout les inputs
                foreach ($valeurs as $var) {
                    echo "<input type='submit' name='del" . $var . "' value='Supprimer' formaction='creer.php'/>";
                }
                echo "</div></td></tr>";
            }

            $newJsonString = json_encode($valeurs, JSON_PRETTY_PRINT);
            file_put_contents('json/values.json', $newJsonString);
            ?>

            <tr>
                <td colspan=2>
                    <div class='labelClass'>
                        <input type="text" name="valueAdd" placeholder="Nouvelle valeur">
                    </div>
                    <div class='inputClass'>
                        <input type="submit" name="Ajouter" value='Ajouter' formaction="creer.php">
                    </div>
                </td>
            </tr>



            <?php
            //On affiche les personnes enregistrées pour repondre au bulletin
            if (count($personneQuestionnaire) > 0) {
                echo "
                        <tr>
                        <td colspan=2>
                        <div class='labelClass'>";
                foreach ($personneQuestionnaire as $var) {
                    echo "<label for='" . $var["id"] . "'>" . $var["nom"] . " " . $var["prenom"] . "</label>";
                }
                echo "</div><div class='inputClass'>";
                foreach ($personneQuestionnaire as $var) {
                    echo  "<input type='submit' name='del" . $var["id"] . "' value='Supprimer' formaction='creer.php'/>";
                }
                echo "
                        </div>
                        </td>
                        </tr>";
            }

            //On rencode les personne qui participeront au questionnaire
            $variable = json_encode($personneQuestionnaire, JSON_PRETTY_PRINT);
            file_put_contents('json/personneQuestionnaire.json', $variable);
            echo "<tr>
                    <td colspan='2'>
                        <div class='labelClass'>
                        <select name='people' id='peopleSelect'>";
            if ($personne != null) {
                foreach ($personne as $var) {
                    /*Cette condition permet de tester si la personne que l'on va afficher est deja dans la liste des options
                            Si elle l'est deja, on ne l'affiche pas*/
                    if (!fetchPersonneBool($var["id"], $personneQuestionnaire)) {
                        echo "<option value='" . $var["id"] . "'>" . $var["nom"] . " " . $var["prenom"] . "</option>";
                    }
                }
            }
            echo "
                    </select>
                    </div>
                    <div class='inputClass'>
                    <input type='submit' name='addPersonne' value='Ajouter'>
                    </div>
                    </td>
                    </tr>
                    ";
            ?>
            <tr>
                <td>
                    <button type="reset" value="Reset">Reset</button>
                </td>
                <td>
                    <button type='submit' name='accueil' formaction='accueil.php'>Accueil</button>
                </td>
            <tr>
                <td colspan='2'>
                    <button type="submit" name="creer" value="Creer">Creer</button>
                </td>
            </tr>
            </tr>
        </table>
    </form>


</body>

</html>