<?php
session_start();
$jsonString = file_get_contents('json/bulletin.json');
$bulletinJson = json_decode($jsonString, true);
function fetchPersonne($id, $personne)
{
    foreach ($personne as $var) {
        if ($id == $var["id"]) {
            return $var;
        }
    }
}
function fetchPersonneBool($id, $tableau)
{
    foreach ($tableau as $var) {
        if ($id == $var["id"]) {
            return True;
        }
    }
    return False;
}
function fetchBulletin($id, $bulletin)
{
    foreach ($bulletin as $var) {
        if ($id == $var["id"]) {
            return $var;
        }
    }
}

if (isset($_POST["idBulletinDel"])) {
    //Si on a appuyé sur un bouton suppr on supprime l'id caché dans l'input hidden
    foreach ($bulletinJson as $key => $bull) {
        //Si on a trouvé le bulletin ciblé par la supression
        if ($bull["id"] == $_POST["idBulletinDel"]) {
            //On supprime le bulletin, on rencode la supression et on redirige l'utilisateur vers la page de succes
            unset($bulletinJson[$key]);
            $variable = json_encode($bulletinJson, JSON_PRETTY_PRINT);
            file_put_contents('json/bulletin.json', $variable);
            header('Location: success.php');
            exit();
        }
    }
}
?>


<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>Bulltozer</title>
    <link rel="stylesheet" href="css/main.css">
</head>




<body>
    <header>
        <h1>Bulltozer</h1>
        <h2>Gerer</h2>
    </header>


    <?php
    //Si verified false on redirige vers la page de login
    if (!$_SESSION["verified"]) {
        header('Location: login.php');
        exit();
    }
    // Si on vient de l'accueil
    if (isset($_POST["gererAccueil"])) {
        //Debut du formulaire
        echo "<form name='bulletinSelection' action='gerer.php' method='POST'>
    <table id='bulletinPresentation'>";

        echo "<label for='bulletinSelect'>Lequel :</label>";
        echo "<select class='inputCentre' name='bulletin' id='bulletinSelect'>";
        if (count($bulletinJson) != 0) {
            foreach ($bulletinJson as $var) {
                //On affiche les bulletin ou l'utilisateur a le droit de repondre. Pour ca on verifie que la personne existe dans les personnes autorisée.
                if (fetchPersonneBool($_SESSION["personne"]["id"], $var["personnes"]) || $var["createur"]["id"] == $_SESSION["personne"]["id"]) {
                    //Ici on change la couleur du select en fonction de si il est complet ou non
                    if ($var["complet"]) {
                        echo "<option style='color:#eb4c34' value='" . $var["id"] . "'>" . $var["question"] . "</option>";
                    } else {
                        echo "<option style='color:#3aeb34' value='" . $var["id"] . "'>" . $var["question"] . "</option>";
                    }
                }
            }
        }
        echo "</select>";

        //Fin du formulaire et bouton
        echo "<tr>
                <td>
                    <button type='submit' name='gerer' value='Gerer' formaction='gerer.php'/>Gerer</button>
                </td>
                <td>
                    <button type='submit' name='accueil' formaction='accueil.php'>Accueil</button>
                </td>
            </tr>
            </table>
            </form>";
    } else {
        //Sinon si on a bien séléctionné un bulletin
        if (isset($_POST["bulletin"])) {
            $fetchBulletin = fetchBulletin($_POST["bulletin"], $bulletinJson);
            echo "<h3>" . $fetchBulletin["question"] . "</h3>";
            //Dans le cas ou tout le monde n'a pas repondu
            if (!$fetchBulletin["complet"]) {
                echo "<p>Tout le monde n'a pas voté</p>
                <form name='bulletinManquant' action='gerer.php' method='POST'>
                <table>
                <tr>
                <td colspan='2' id='nomPasVote'>";
                //On parcours les personnes qui ont le droit de repondre
                foreach ($fetchBulletin["personnes"] as $pers) {
                    $found = False;
                    //On parcours les votes
                    foreach ($fetchBulletin["votes"] as $vote) {
                        //Si l'id du vote correspond a l'id de la personne c'est qu'elle a voté
                        if ($vote["id"] == $pers["id"]) {
                            $found = True;
                        }
                    }
                    //Si on a pas trouvé la personne dans la liste des votants alors on l'affiche
                    if (!$found) {
                        echo $pers["nom"] . " " . $pers["prenom"] . "<br>";
                    }
                }
                echo
                "</td>
                </tr>
                <tr>
                <td>
                <button type='submit' name='accueil' formaction='accueil.php'>Accueil</button>
                </td>";
                //Si l'id de la session est le meme que l'id du createur on créé un bouton supprimé
                if ($fetchBulletin["createur"]["id"] == $_SESSION["personne"]["id"]) {
                    echo "<td><button type='submit' name='suppr' formaction='gerer.php'>Suprimer</button>
                    <input type='hidden' name='idBulletinDel' value='" . $fetchBulletin["id"] . "'></td>";
                }
                echo "</tr>
                </table>
                </form>";
            }
            //Dans le cas ou tout le monde a voté
            else {
                echo "<br>Tout le monde a voté";
                $reponses = array();
                //On parcours les votes
                foreach ($fetchBulletin["votes"] as $votes) {
                    //Si la clé existe on lui ajoute un
                    if (array_key_exists($votes["valeur"], $reponses)) {
                        $reponses[$votes["valeur"]] = $reponses[$votes["valeur"]] + 1;
                    }
                    //Sinon on créé la cle qu'on initialise a 1
                    else {
                        $reponses[$votes["valeur"]] = 1;
                    }
                }
                echo "<form name='bulletinManquant' action='gerer.php' method='POST'>
                <table>
                <tr>
                <td colspan='2'>";
                //Pour chaque reponse dans le nouveau tableau, on affiche les statistiques
                echo "<div class='labelClass'>";
                foreach ($reponses as $key => $var) {
                    echo "<p>".$key ."</p>";
                }
                echo"</div>";
                echo "<div class='inputClass'>";
                foreach ($reponses as $key => $var) {
                    echo "<p>". $var . "</p>";
                }
                echo "</div>";
                echo "</td>
                </tr>
                <tr>
                <td>
                <button type='submit' name='accueil' formaction='accueil.php'>Accueil</button>
                </td>";
                //Si l'id de la session est le meme que l'id du createur on créé un bouton supprimé
                if ($fetchBulletin["createur"]["id"] == $_SESSION["personne"]["id"]) {
                    echo "
                    <td>
                    <button type='submit' name='suppr' formaction='gerer.php'>Suprimer</button>
                    <input type='hidden' name='idBulletinDel' value='" . $fetchBulletin["id"] . "'></td>";
                }
                echo "</tr>
                </table>
                </form>";
            }
        }
    }
    ?>

</body>

</html>