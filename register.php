<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>Bulltozer</title>
    <link rel="stylesheet" href="css/main.css">
</head>




<body>
    <header>
        <h1>Bulltozer</h1>
        <h2>Register</h2>
    </header>

    <?php
    //On test ici si l'adresse mail renseigné est deja utilisé
    if (isset($_POST["register"])) {
        if (empty($_POST["email"]) || empty($_POST["nom"]) || empty($_POST["prenom"]) || empty($_POST["password"])) {
            echo "<p>Il faut remplir tout les champs</p>";
        } else {
            $jsonString = file_get_contents('json/personne.json');
            $data = json_decode($jsonString, true);
            //On créé un tableau avec les valeurs du formulaire (pas besoin de les tester elles sont en required), on hash le password et on push dans data
            $test = true;
            foreach ($data as $var) {
                if ($var["email"] === $_POST["email"]) {
                    $test = false;
                    break;
                }
            }
            if ($test) {
                if (count($data) == 0) {
                    $data[] = array("id" => "0", "nom" => $_POST["nom"], "prenom" => $_POST["prenom"], "email" => $_POST["email"], "password" => password_hash($_POST["password"], PASSWORD_DEFAULT));
                } else {
                    $data[] = array("id" => (string)(end($data)["id"] + 1), "nom" => $_POST["nom"], "prenom" => $_POST["prenom"], "email" => $_POST["email"], "password" => password_hash($_POST["password"], PASSWORD_DEFAULT));
                }
                $newJsonString = json_encode($data, JSON_PRETTY_PRINT);
                file_put_contents('json/personne.json', $newJsonString);
                header('Location: success.php');
                exit();
            } else {
                echo "<p>Cette adresse mail est deja utilisé</p>";
            }
        }
    }
    ?>

    <form name="connexion" action="register.php" method="POST">
        <table>
            <tr>
                <td colspan='2'>
                    <div class="labelClass">
                        <label for="nom">Nom</label>
                        <label for="prenom">Prenom</label>
                        <label for="email">Email</label>
                        <label for="password">Password</label>
                    </div>
                    <div class="inputClass">
                        <input type="text" name="nom" placeholder="Nom">
                        <input type="text" name="prenom" placeholder="Prenom">
                        <input type="email" name="email" placeholder="Email">
                        <input type="password" name="password" placeholder="Password">
                    </div>
                </td>
            </tr>
            <tr>
                    <td>
                        <button type="submit" name="register" value="S'inscrire">S'inscrire</button>
                    </td>
                    <td>
                        <button type="submit" name="accueil" value="Accueil" formaction="accueil.php">Accueil</button>
                    </td>

            </tr>
        </table>
    </form>
</body>

</html>