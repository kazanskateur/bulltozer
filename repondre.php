<?php
session_start();
$jsonString = file_get_contents('json/bulletin.json');
$bulletinJson = json_decode($jsonString, true);
function fetchPersonne($id, $personne)
{
    foreach ($personne as $var) {
        if ($id == $var["id"]) {
            return $var;
        }
    }
}
function fetchPersonneBool($id, $tableau)
{
    foreach ($tableau as $var) {
        if ($id == $var["id"]) {
            return True;
        }
    }
    return False;
}
function fetchBulletin($id, $bulletin)
{
    foreach ($bulletin as $var) {
        if ($id == $var["id"]) {
            return $var;
        }
    }
}
?>


<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>Bulltozer</title>
    <link rel="stylesheet" href="css/main.css">
</head>




<body>
    <header>
        <h1>Bulltozer</h1>
        <h2>Repondre</h2>
    </header>


    <?php
    //Si verified false on redirige vers la page de login
    if (!$_SESSION["verified"]) {
        header('Location: login.php');
        exit();
    }
    // Si on vient de l'accueil
    if (isset($_POST["repondreAccueil"])) {
        //Debut du formulaire
        echo "
            <form name='bulletinSelection' action='repondre.php' method='POST'>
            <table id='bulletinPresentation'>
            <label for='bulletinSelect'>Lequel :</label>
            <select class='inputCentre' name='bulletin' id='bulletinSelect'>";
        if (count($bulletinJson) != 0) {
            foreach ($bulletinJson as $var) {
                //On affiche les bulletin ou l'utilisateur a le droit de repondre. Pour ca on verifie que la personne existe dans les personnes autorisée et n'existe pas dans les personnes ayant repondu.
                if (fetchPersonneBool($_SESSION["personne"]["id"], $var["personnes"]) && !fetchPersonneBool($_SESSION["personne"]["id"], $var["votes"]) && !$var["complet"]) {
                    echo "<option value='" . $var["id"] . "'>" . $var["question"] . "</option>";
                }
            }
        }
        //Fin du formulaire et bouton
        echo "
        </select>
        <tr>
            <td>
                <button type='submit' name='repondre' value='Répondre' formaction='repondre.php' />Repondre</button>
            </td>
            <td>
                <button type='submit' name='accueil' formaction='accueil.php'>Accueil</button>
            </td>
        </tr>
        </table>
        </form>
        ";
    } else {
        //Sinon si on a bien envoyé un bulletin
        if (isset($_POST["bulletin"])) {
            //On recupere depuis la DB le dis bulletin dont l'id est stocké dans la valeur get du bulletin
            $fetchBulletin = fetchBulletin($_POST["bulletin"], $bulletinJson);
            //On affiche la question ainsi que le formulaire
            echo 
            "<p>".
            $fetchBulletin["question"] .
            "</p>
            <form name='bulletinReponse' action='repondre.php' method='POST'>
            <table>
            <tr>
            <td colspan='2'>";
            //Pour chaque options du bulletin on affiche la valeur
            echo"<div class='labelClass'>";
            foreach ($fetchBulletin["options"] as $var) {
                echo "<label for='" . $var . "'>" . $var . "</label>";
            }
            echo"</div><div class='inputClass'>";
            foreach ($fetchBulletin["options"] as $var) {
                echo "<input type='radio' name='options' value='" . $var . "'checked>";
            }
            echo "
            </div>
            </td>
            </tr>
            <tr>
                <td>
                    <button type='submit' name='repondre' value='Répondre' formaction='repondre.php' />Repondre</button>
                </td>
                <td>
                    <button type='submit' name='accueil' formaction='accueil.php'>Accueil</button>
                </td>
            </tr>
            </table>
            <input type='hidden' name='idBulletin' value='" . $fetchBulletin["id"] . "'>
            </form>
            ";
        } 
        //Sinon si la valeurs de options est set, c'est qu'on a fait un choix
        elseif (isset($_POST["options"])) {
            foreach ($bulletinJson as $key => $var) {
                //Si les id concordent
                if ($var["id"] == $_POST["idBulletin"]) {
                    //on ajoute au tableau de vote
                    $bulletinJson[$key]["votes"][] = array("id" => $_SESSION["personne"]["id"], "valeur" => $_POST["options"]);
                    //Si le nombre de reponse est egal au nombre de personne pouvant repondre, le questionnaire est plein
                    if (count($bulletinJson[$key]["votes"]) == count($bulletinJson[$key]["personnes"])) {
                        $bulletinJson[$key]["complet"] = true;
                    }
                }
            }
            //On rencode les informations ajoutées
            $variable = json_encode($bulletinJson, JSON_PRETTY_PRINT);
            file_put_contents('json/bulletin.json', $variable);
            //On redirige vers la page succes
            header('Location: success.php');
            exit();
        }
        //Sinon on a acceder a la page sans passer par la page ou l'accueil il y'a donc une erreur
        else {
            header('Location: error.php');
            exit();
        }
    }
    ?>

</body>

</html>