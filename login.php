<?php
session_start();
$jsonString = file_get_contents('json/personne.json');
$personne = json_decode($jsonString, true);

if (isset($_POST["gererAccueil"])) {
    $_SESSION["origine"] = "gerer";
} elseif (isset($_POST["repondreAccueil"])) {
    $_SESSION["origine"] = "repondre";
} elseif (isset($_POST["creerAccueil"])) {
    $_SESSION["origine"] = "creer";
} elseif (isset($_POST["accountAccueil"])) {
    $_SESSION["origine"] = "account";
}


if ($_SESSION["verified"]) {
    header('Location: accueil.php');
    exit();
} else {
    if (isset($_POST["connect"])) {
        $test = false;
        if (empty($_POST["email"]) || empty($_POST["password"])) {
            $erreurLog = "Champs vides";
        } else {
            $mdpTest = $_POST["password"];
            $mail = $_POST["email"];

            foreach ($personne as $var) {
                if (password_verify($mdpTest, $var["password"]) && $mail == $var["email"]) {
                    $test = true;
                    break;
                }
            }
            if ($test) {
                $_SESSION["verified"] = true;
                $_SESSION["personne"] = fetchPersonneEmail($_POST["email"], $personne);
                header('Location:' . $_SESSION["origine"] . '.php');
                exit();
            } else {
                $erreurLog = "Mauvaise combinaison";
            }
        }
    }
}

function fetchPersonneEmail($email, $personne)
{
    foreach ($personne as $var) {
        if ($email == $var["email"]) {
            return $var;
        }
    }
}
?>



<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>Bulltozer</title>
    <link rel="stylesheet" href="css/main.css">
</head>




<body>
    <header>
        <h1>Bulltozer</h1>
        <h2>Login</h2>
    </header>
    <?php
    if(isset($erreurLog)){
        echo '<p>' . $erreurLog . '</p>';
    }
    ?>


    <form name="connexion" action="login.php" method="POST">
        <table>
            <tr>
                <td colspan='2'>
                    <div class="labelClass">
                        <label for="email">Email</label>
                        <label for="password">Password</label>
                    </div>
                    <div class="inputClass">
                        <input type="text" name="email" placeholder="Email">
                        <input type="password" name="password" placeholder="Password">
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <button type="submit" name="connect" value="Connection">Connection</button>
                </td>
                <td colspan='2'>
                    <button type='submit' name='accueil' formaction='accueil.php'>Accueil</button>
                </td>
            </tr>
        </table>
    </form>


</body>

</html>